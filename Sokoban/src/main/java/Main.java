import presenter.Presenter;
import view.Frame;
import view.MenuFrame;


public class Main {
    
    public static void main(String[] args) {
        Frame frame = new Frame();
        Presenter presenter = new Presenter();
        presenter.setFrame(frame);
        frame.attachPresenter(presenter);
        MenuFrame menuFrame = new MenuFrame(frame, presenter);
        menuFrame.setVisible(true);
    }
}
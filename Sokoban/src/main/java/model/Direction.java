package model;

public record Direction(int x, int y) {
}

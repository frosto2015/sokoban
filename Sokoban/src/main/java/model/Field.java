package model;


import org.jetbrains.annotations.NotNull;

public final class Field {
    private final GameElement[][] grid;
    private final int targetCount;
    private int boxesOnTargetsCount;
    private final Position playerPosition;
    private int moveCounter;
    private boolean isWinning = false;
    private final int height;
    private final int width;

    public Field(GameElement[][] grid, int targetCount, int boxesOnTargetsCount, Position playerPosition) {
        this.grid = grid;
        this.targetCount = targetCount;
        this.boxesOnTargetsCount = boxesOnTargetsCount;
        this.playerPosition = playerPosition;
        this.height = grid.length;
        this.width = grid[0].length;
    }

    /**
     * Updates the game state by moving the player in the specified direction.
     *
     * @param direction The direction of the player's movement.
     * @throws IllegalArgumentException if the direction is not one of the four cardinal directions (up, down, left, right).
     */
    public void update(Direction direction) {
        if(isWinning){
            return;
        }
        if((direction.x() * direction.y()) != 0 || (direction.x() + direction.y()) > 1 || (direction.x() + direction.y()) < -1){
            throw new IllegalArgumentException();
        }
        moveGameElement(direction, playerPosition);
        if(boxesOnTargetsCount == targetCount){
            isWinning = true;
        }
    }

    private boolean moveGameElement(@NotNull Direction direction, @NotNull Position position) {
        Position nextElementPosition = new Position((position.getX() + direction.x() + width) % width,
                (position.getY() + direction.y() + height) % height);
        GameElement nextElem = grid[nextElementPosition.getY()][nextElementPosition.getX()];
        GameElement curElem = grid[position.getY()][position.getX()];
        if (curElem == GameElement.PLAYER || curElem == GameElement.PLAYER_ON_TARGET) {
            if (nextElem == GameElement.WALL) {
                return false;
            }
            if (nextElem.isMovable()) {
                if (!moveGameElement(direction, nextElementPosition)) {
                    return false;
                }
                nextElem = grid[nextElementPosition.getY()][nextElementPosition.getX()];
            }
            if (curElem == GameElement.PLAYER_ON_TARGET) {
                grid[position.getY()][position.getX()] = GameElement.TARGET;
            } else {
                assert(curElem == GameElement.PLAYER);
                grid[position.getY()][position.getX()] = GameElement.FLOOR;
            }
            assert nextElem == GameElement.TARGET || nextElem == GameElement.FLOOR;
            if (nextElem == GameElement.FLOOR) {
                grid[nextElementPosition.getY()][nextElementPosition.getX()] = GameElement.PLAYER;
            } else {
                assert(nextElem == GameElement.TARGET);
                grid[nextElementPosition.getY()][nextElementPosition.getX()] = GameElement.PLAYER_ON_TARGET;
            }
            playerPosition.setPosition(nextElementPosition.getX(), nextElementPosition.getY());
            moveCounter++;
            return true;
        }
        assert curElem == GameElement.BOX || curElem == GameElement.BOX_ON_TARGET;
        if ((nextElem == GameElement.TARGET) || (nextElem == GameElement.FLOOR)) {
            if (curElem == GameElement.BOX_ON_TARGET) {
                grid[position.getY()][position.getX()] = GameElement.TARGET;
                boxesOnTargetsCount--;
            } else {
                assert(curElem == GameElement.BOX);
                grid[position.getY()][position.getX()] = GameElement.FLOOR;
            }
            if (nextElem == GameElement.TARGET) {
                grid[nextElementPosition.getY()][nextElementPosition.getX()] = GameElement.BOX_ON_TARGET;
                boxesOnTargetsCount++;
            } else {
                assert(nextElem == GameElement.FLOOR);
                grid[nextElementPosition.getY()][nextElementPosition.getX()] = GameElement.BOX;
            }
            return true;
        }
        return false;
    }
    public GameElement[][] getMap() {
        return grid;
    }

    public boolean isWinning() {
        return isWinning;
    }

    public int getMoveCounter() {
        return moveCounter;
    }
}

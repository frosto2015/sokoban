package model;

import org.jetbrains.annotations.NotNull;

public enum GameElement {
    WALL('w'),
    FLOOR('0'),
    TARGET('f'),
    BOX('b'),
    BOX_ON_TARGET('+'),
    PLAYER('p'),
    PLAYER_ON_TARGET('-');
    private final char symbol;


    GameElement(char symbol){
        this.symbol = symbol;

    }
    public boolean isMovable(){
        return this == BOX || this == PLAYER || this == BOX_ON_TARGET;
    }

    public static @NotNull GameElement fromSymbol(char symbol) {
        for (GameElement element : GameElement.values()) {
            if (element.symbol == symbol) {
                return element;
            }
        }
        throw new IllegalArgumentException("Invalid symbol: " + symbol);
    }

}

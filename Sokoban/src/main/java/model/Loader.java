package model;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import utils.MapBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Loader {

    @Contract("_ -> new")
    static public @NotNull Field loadLevel(int level) throws IOException {
        String levelPath = "/levels/" + level + "lvl";
        MapBuilder mapBuilder = new MapBuilder();
        try (InputStream inputStream = Loader.class.getResourceAsStream(levelPath)) {
            mapBuilder.parse(inputStream);
        }
        return new Field(mapBuilder.getField(), mapBuilder.getTargetsSize(), mapBuilder.getBoxesOnTargetsSize(),
                mapBuilder.getPlayerPosition());
    }

}

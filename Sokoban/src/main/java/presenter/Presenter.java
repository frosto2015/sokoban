package presenter;

import model.Direction;
import model.Field;
import model.Loader;
import utils.RecordWriter;
import view.FinalFrame;
import view.Frame;
import view.RecordFrame;

import javax.swing.*;
import java.io.IOException;

public class Presenter implements SokobanClickListener{
    private Frame frame;
    private Field field;

    private int level;

    public Presenter(){}

    public void setFrame(Frame frame)
    {
        this.frame = frame;
    }


    public void restart() throws IOException {
        field = Loader.loadLevel(level);
        frame.attachPresenter(this);
        frame.start(field.getMap());
    }

    @Override
    public void update(Direction direction) {
        boolean currentIsWinning = field.isWinning();
        field.update(direction);
        frame.update(field.getMap());
        if(field.isWinning() && !currentIsWinning){
            FinalFrame finalFrame = new FinalFrame(frame);
            finalFrame.setVisible(true);
            RecordWriter recordWriter = new RecordWriter(level);
            try {
                recordWriter.writeRecord(finalFrame.getPlayerName(), field.getMoveCounter());
            } catch (IOException e) {
                showError("can't write record");
            }
        }
    }
    public void getRecords(){
        RecordWriter recordWriter = new RecordWriter(level);
        try {
            RecordFrame recordFrame = new RecordFrame(recordWriter.readRecords());
            recordFrame.setVisible(true);
        } catch (IOException e) {
            showError("can't get record");
        }

    }
    public void setLevel(int level) {
        this.level = level;
    }
    private void showError(String message) {
        JOptionPane.showMessageDialog(frame, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
}

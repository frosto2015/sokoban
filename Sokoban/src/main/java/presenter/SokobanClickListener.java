package presenter;

import model.Direction;

public interface SokobanClickListener {
    void update(Direction direction);
}

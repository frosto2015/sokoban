package utils;

import model.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class MapBuilder {
    private GameElement[][] field;
    private int targetsSize = 0;
    private int boxesOnTargetsSize = 0;

    private Position position;

    public void parse(InputStream inputStream) {
        GameElement[][] emptyField = new GameElement[1][1];
        emptyField[0][0] = GameElement.PLAYER;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        List<String> map = reader.lines().toList();
        boolean allIntegers = Arrays.stream(map.get(0).split(" ")).allMatch(element ->{
            try{
                Integer.parseInt(element);
            }catch (IllegalArgumentException e){
                System.out.println("invalid argument of size " + element);
                return false;
            }
            return true;
        });
        if(!allIntegers){
            field = emptyField;
            return;
        }
        int[] size = Arrays.stream(map.get(0).split(" ")).mapToInt(Integer::parseInt).toArray();
        int width = size[0];
        int height = size[1];
        targetsSize = size[2];
        boxesOnTargetsSize = size[3];
        GameElement[][] localField = new GameElement[height][width];
        for(int i = 0; i < height; i++) {
            String line;
            try{
                line = map.get(i+1);
            }catch (IndexOutOfBoundsException e){
                System.out.println("out of bounds height");
                field = emptyField;
                return;
            }
            for (int j = 0; j < width; j++){
                try{
                    localField[i][j] = GameElement.fromSymbol(line.charAt(j));
                }catch (IndexOutOfBoundsException e){
                    System.out.println("out of bounds width");
                    field = emptyField;
                    return;
                }
                catch (IllegalArgumentException e){
                    System.out.println("invalid map element " + localField[i][j]);
                    field = emptyField;
                    return;
                }
                if(localField[i][j] == GameElement.PLAYER){
                    position = new Position(j, i);
                }
            }
        }
        field = localField;
    }
    public GameElement[][] getField()
    {
        return field;
    }

    public int getTargetsSize() {
        return targetsSize;
    }

    public int getBoxesOnTargetsSize() {
        return boxesOnTargetsSize;
    }

    public Position getPlayerPosition() {
        return position;
    }
}

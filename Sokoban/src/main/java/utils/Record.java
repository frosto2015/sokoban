package utils;

public record Record(String playerName, int score) {
}

package utils;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public record RecordWriter(int level) {
    private static final int MAX_RECORDS = 10;
    private static final String RECORDS_DIRECTORY = "records/";

    public @NotNull List<Record> readRecords() throws IOException {
        List<Record> records = new ArrayList<>();
        Path recordPath = getRecordPath();
        if (!Files.exists(recordPath)) {
            return records;
        }
        List<String> lines = Files.readAllLines(recordPath);
        for (String line : lines) {
            String[] parts = line.split(" ");
            if (parts.length == 2) {
                String playerName = parts[0];
                int score = Integer.parseInt(parts[1]);
                records.add(new Record(playerName, score));
            }
        }
        return records;
    }
    public void writeRecord(String playerName, int score) throws IOException {
        List<Record> records = readRecords();
        Path recordPath = getRecordPath();
        records.add(new Record(playerName, score));
        records.sort(Comparator.comparingInt(Record::score));
        if (records.size() > MAX_RECORDS) {
            records = records.subList(0, MAX_RECORDS);
        }
        Files.createDirectories(recordPath.getParent());
        Files.writeString(recordPath, "");
        for(Record record : records){
            String line = record.playerName() + " " + record.score();
            line += System.lineSeparator();
            Files.writeString(recordPath, line, StandardOpenOption.APPEND);
        }
    }
    private Path getRecordPath() {
        return Paths.get(RECORDS_DIRECTORY + level + "lvl");
    }

}

package view;

import javax.swing.*;
import java.awt.*;


public class AboutFrame extends JFrame{
    AboutFrame(){
        this.setTitle("About");
        this.setLayout(new GridBagLayout());
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setPreferredSize(new Dimension(450, 500));
        JPanel panel = new JPanel();
        JLabel jlabel = new JLabel(" ¯\\_(ツ)_/¯");
        panel.add(jlabel);
        this.add(panel, new GridBagConstraints());
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}

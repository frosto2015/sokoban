package view;



import model.GameElement;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;


public class FieldPanel extends JPanel
{
    public GameElement[][] field;
    private int width;
    private int height;
    private static final int CELL_SIZE = 50;
    private static final HashMap<GameElement, BufferedImage> elementImages = new HashMap<>();
    static {
        elementImages.put(GameElement.WALL, loadImage("/images/wall.png"));
        elementImages.put(GameElement.FLOOR, loadImage("/images/floor.png"));
        elementImages.put(GameElement.TARGET, loadImage("/images/target.png"));
        elementImages.put(GameElement.BOX, loadImage("/images/box.png"));
        elementImages.put(GameElement.BOX_ON_TARGET, loadImage("/images/boxOnTarget.png"));
        elementImages.put(GameElement.PLAYER, loadImage("/images/player.png"));
        elementImages.put(GameElement.PLAYER_ON_TARGET, loadImage("/images/player.png"));
    }
    private static BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(Objects.requireNonNull(FieldPanel.class.getResourceAsStream(path)));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load image: " + path, e);
        }
    }
    public void setField(GameElement[][] field) {
        this.field = field;
        this.height = field.length;
        this.width = field[0].length;
        this.repaint();
    }
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                g.drawImage(elementImages.get(field[row][col]), col * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE, this);
            }
        }
    }
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width * CELL_SIZE, height * CELL_SIZE);
    }

    public void clearField() {
        field = null;
        repaint();
    }
}

package view;

import javax.swing.*;
import java.awt.*;

public class FinalFrame extends JDialog {
    private String playerName;
    public FinalFrame(JFrame parentFrame) {
        super(parentFrame, "Victory", true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setSize(400, 200);
        setLocationRelativeTo(null);
        JLabel nameLabel = new JLabel("Write tour name");
        JTextField nameTextField = new JTextField(20);
        JButton  submitButton = new JButton("Send");
        submitButton.addActionListener(e -> {
            playerName = nameTextField.getText().trim();
            dispose();
        });
        setLayout(new FlowLayout());
        add(nameLabel);
        add(nameTextField);
        add(submitButton);
        setModal(true);
    }

    public String getPlayerName() {
        return playerName;
    }
}

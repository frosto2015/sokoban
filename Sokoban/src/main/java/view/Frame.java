package view;

import model.Direction;
import model.GameElement;
import org.jetbrains.annotations.NotNull;
import presenter.Presenter;

import javax.swing.*;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

public class Frame extends JFrame implements KeyListener {

    public FieldPanel fieldPanel;
    private static final String MENU = "Menu";
    private static final String RESTART = "Restart";
    private static final String EXIT = "Exit";
    private static final String RECORDS = "Records";
    private static final String ABOUT = "About";
    private static final String CHOOSE_LEVEL = "Choose level";
    private Presenter presenter;

    public Frame()
    {
        this.setTitle("Sokoban");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu(MENU);
        JMenuItem restartItem = new JMenuItem(RESTART);
        JMenuItem exitItem = new JMenuItem(EXIT);
        JMenuItem aboutItem = new JMenuItem(ABOUT);
        JMenuItem recordsItem = new JMenuItem(RECORDS);
        JMenuItem chooseItem = new JMenuItem(CHOOSE_LEVEL);
        chooseItem.addActionListener(event -> new MenuFrame(this, presenter).setVisible(true));
        exitItem.addActionListener(event -> System.exit(0));
        aboutItem.addActionListener(event -> new AboutFrame().setVisible(true));
        restartItem.addActionListener(event -> {
            try {
                presenter.restart();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        recordsItem.addActionListener(event -> presenter.getRecords());
        addKeyListener(this);
        menu.add(restartItem);
        menu.add(recordsItem);
        menu.add(aboutItem);
        menu.add(chooseItem);
        menu.add(exitItem);
        menuBar.add(menu);
        this.setJMenuBar(menuBar);

    }
    public void attachPresenter(Presenter presenter) {
        this.presenter = presenter;
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(@NotNull KeyEvent e) {
        handleKeyPress(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void start(GameElement[][] field) {
        fieldPanel = new FieldPanel();
        fieldPanel.setField(field);
        this.add(fieldPanel);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
    }

    public void update(GameElement[][] field) {
        fieldPanel.setField(field);
    }
    public void handleKeyPress(int keyCode)
    {
        switch(keyCode)
        {
            case KeyEvent.VK_UP -> presenter.update(new Direction(0, -1));
            case KeyEvent.VK_DOWN -> presenter.update(new Direction(0, 1));
            case KeyEvent.VK_LEFT -> presenter.update(new Direction(-1, 0));
            case KeyEvent.VK_RIGHT -> presenter.update(new Direction(1, 0));
        }
    }
    public void clearField() {
        if (fieldPanel != null) {
            fieldPanel.clearField();
            remove(fieldPanel);
            fieldPanel = null;
        }
    }
}

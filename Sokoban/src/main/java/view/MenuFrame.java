package view;

import presenter.Presenter;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class MenuFrame extends JFrame{

    public MenuFrame(Frame frame, Presenter presenter){
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(300, 200);
        this.setLayout(new FlowLayout());
        JLabel label = new JLabel("Choose level");
        this.add(label);
        JList<String> levelList = new JList<>(new String[]{"Level 1", "Level 2", "Level 3"});
        levelList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        levelList.setVisibleRowCount(3);
        levelList.setFixedCellHeight(50);
        JScrollPane scrollPane = new JScrollPane(levelList);
        this.add(scrollPane);

        levelList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                int level = levelList.getSelectedIndex() + 1;
                try {
                    presenter.setLevel(level);
                    frame.clearField();
                    presenter.setFrame(frame);
                    presenter.restart();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
                this.dispose();
            }
        });
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}

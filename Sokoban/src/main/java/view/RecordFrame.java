package view;

import org.jetbrains.annotations.NotNull;
import utils.Record;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;
public class RecordFrame extends JFrame {

    public RecordFrame(@NotNull List<Record> records) {
        setTitle("Таблица рекордов");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(400, 300);
        setLocationRelativeTo(null);

        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("Player");
        tableModel.addColumn("Score");

        for(Record record : records){
            tableModel.addRow(new Object[]{record.playerName(), record.score()});
        }

        JTable recordsTable = new JTable(tableModel);

        JScrollPane scrollPane = new JScrollPane(recordsTable);

        getContentPane().add(scrollPane);
    }
}

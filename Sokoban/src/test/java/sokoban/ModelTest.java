package sokoban;

import model.Direction;
import model.Field;
import model.Position;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sokoban.core.Comparator;
import sokoban.core.Parser;

public class ModelTest {
    @Test
    public void nextFloorLeft(){
        String map = """
                5 4
                wwwww
                wf00w
                w00pw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0p0w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextFloorRight(){
        String map = """
                5 4
                wwwww
                wf00w
                w00p0
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w000p
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextFloorUp(){
        String map = """
                5 4
                wwwww
                wf00w
                w00pw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf0pw
                w000w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextFloorDown(){
        String map = """
                5 4
                wwwww
                wf00w
                w00pw
                www0w
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w000w
                wwwpw
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxDown(){
        String map = """
                5 4
                wwwww
                wfp0w
                w0b0w
                ww0ww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0p0w
                wwbww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxLeft(){
        String map = """
                5 4
                wwwww
                wf00w
                w0bpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                wbp0w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxRight(){
        String map = """
                5 4
                wwwww
                wf00w
                w0pb0
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w00pb
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxUp(){
        String map = """
                5 4
                www0w
                wf0bw
                w00pw
                wwwww
                """;
        String map1 = """
                5 4
                wwwbw
                wf0pw
                w000w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextWallLeft(){
        String map = """
                5 4
                wwwww
                wf00w
                w0wpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0wpw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextWallRight(){
        String map = """
                5 4
                wwwww
                wf00w
                w0wpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0wpw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextWallUp(){
        String map = """
                5 4
                wwwww
                wf0ww
                w0wpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf0ww
                w0wpw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextWallDown(){
        String map = """
                5 4
                wwwww
                wf00w
                w0wpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0wpw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextTargetLeft(){
        String map = """
                5 4
                wwwww
                wf00w
                w0fpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0-0w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 2, 0, new Position(3, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));

    }
    @Test
    public void nextTargetRight(){
        String map = """
                5 4
                wwwww
                wf00w
                w00pf
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w000-
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 2, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));

    }
    @Test
    public void nextTargetUp(){
        String map = """
                5 4
                wwwww
                wf0fw
                w00pw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf0-w
                w000w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 2, 0, new Position(3, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));

    }
    @Test
    public void nextTargetDown(){
        String map = """
                5 4
                wwwww
                wf00w
                w0fpw
                wwwfw
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0f0w
                www-w
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 2, 0, new Position(3, 2));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));

    }
    @Test
    public void nextBoxNextTargetLeft(){
        String map = """
                5 4
                wwwww
                w000w
                wfbpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                w000w
                w+p0w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        Assertions.assertTrue(field.isWinning());
    }
    @Test
    public void nextBoxNextTargetRight(){
        String map = """
                5 4
                wwwww
                w000w
                wpbfw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                w000w
                w0p+w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(1, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        Assertions.assertTrue(field.isWinning());
    }
    @Test
    public void nextBoxNextTargetUp(){
        String map = """
                5 4
                wwwfw
                w00bw
                w00pw
                wwwww
                """;
        String map1 = """
                5 4
                www+w
                w00pw
                w000w
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        Assertions.assertTrue(field.isWinning());
    }
    @Test
    public void nextBoxNextTargetDown(){
        String map = """
                5 4
                wwwww
                w00pw
                w00bw
                wwwfw
                """;
        String map1 = """
                5 4
                wwwww
                w000w
                w00pw
                www+w
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        Assertions.assertTrue(field.isWinning());
    }
    @Test
    public void nextBoxNextWallLeft(){
        String map = """
                5 4
                wwwww
                wf00w
                0wbpw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                0wbpw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxNextWallRight(){
        String map = """
                5 4
                wwwww
                wf00w
                w0pbw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf00w
                w0pbw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxNextWallUp(){
        String map = """
                5 4
                wwwww
                wf0bw
                ww0pw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf0bw
                ww0pw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxNextWallDown(){
        String map = """
                5 4
                wwwww
                wf0pw
                ww0bw
                wwwww
                """;
        String map1 = """
                5 4
                wwwww
                wf0pw
                ww0bw
                wwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxNextBoxLeft(){
        String map = """
                6 4
                wwwwww
                wf00ww
                w0bbpw
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wf00ww
                w0bbpw
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(4, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxNextBoxRight(){
        String map = """
                6 4
                wwwwww
                wf00ww
                w0pbb0
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wf00ww
                w0pbb0
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxNextBoxUp(){
        String map = """
                6 4
                wwwwbw
                wf00bw
                w000pw
                wwww0w
                """;
        String map1 = """
                6 4
                wwwwbw
                wf00bw
                w000pw
                wwww0w
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(4, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxNextBoxDown(){
        String map = """
                6 4
                wwwwpw
                wf00bw
                w000bw
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwpw
                wf00bw
                w000bw
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(4, 0));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxOnTargetLeft(){
        String map = """
                6 4
                wwwwww
                wf00ww
                w00+pw
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wf00ww
                w0b-0w
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(4, 2));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxOnTargetRight(){
        String map = """
                6 4
                wwwwww
                wf00ww
                w0p+0w
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wf00ww
                w00-bw
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxOnTargetUp(){
        String map = """
                6 4
                wwww0w
                wf00+w
                w000pw
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwbw
                wf00-w
                w0000w
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(4, 2));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void nextBoxOnTargetDown(){
        String map = """
                6 4
                wwwwww
                wf00pw
                w000+w
                wwww0w
                """;
        String map1 = """
                6 4
                wwwwww
                wf000w
                w000-w
                wwwwbw
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(4, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleFloorLeft() {
        String map = """
                6 4
                wwwwww
                wwwwww
                00p000
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                00000p
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleFloorRight() {
        String map = """
                6 4
                wwwwww
                wwwwww
                000p00
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                p00000
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleFloorUp() {
        String map = """
                6 4
                ww0www
                ww0www
                00p000
                ww0www
                """;
        String map1 = """
                6 4
                ww0www
                ww0www
                000000
                wwpwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleFloorDown() {
        String map = """
                6 4
                ww0www
                ww0www
                00p000
                ww0www
                """;
        String map1 = """
                6 4
                wwpwww
                ww0www
                000000
                ww0www
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleBoxLeft() {
        String map = """
                6 4
                wwwwww
                wwwwww
                b0p000
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                0000bp
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleBoxRight() {
        String map = """
                6 4
                wwwwww
                wwwwww
                000pb0
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                pb0000
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleBoxUp() {
        String map = """
                6 4
                ww0www
                wwbwww
                00p000
                ww0www
                """;
        String map1 = """
                6 4
                ww0www
                ww0www
                00b000
                wwpwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleBoxDown() {
        String map = """
                6 4
                ww0www
                ww0www
                00p000
                wwbwww
                """;
        String map1 = """
                6 4
                wwpwww
                wwbwww
                000000
                ww0www
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleWallLeft() {
        String map = """
                6 4
                wwwwww
                wwwwww
                00p00w
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                p0000w
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleWallRight() {
        String map = """
                6 4
                wwwwww
                wwwwww
                w00p00
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                w0000p
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleWallUp() {
        String map = """
                6 4
                ww0www
                ww0www
                00p000
                wwwwww
                """;
        String map1 = """
                6 4
                wwpwww
                ww0www
                000000
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleWallDown() {
        String map = """
                6 4
                wwwwww
                ww0www
                00p000
                ww0www
                """;
        String map1 = """
                6 4
                wwwwww
                ww0www
                000000
                wwpwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleTargetLeft() {
        String map = """
                6 4
                wwwwww
                wwwwww
                00p00f
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                00000-
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleTargetRight() {
        String map = """
                6 4
                wwwwww
                wwwwww
                f00p00
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                -00000
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleTargetUp() {
        String map = """
                6 4
                ww0www
                ww0www
                00p000
                wwfwww
                """;
        String map1 = """
                6 4
                ww0www
                ww0www
                000000
                ww-www
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleTargetDown() {
        String map = """
                6 4
                wwfwww
                ww0www
                00p000
                ww0www
                """;
        String map1 = """
                6 4
                ww-www
                ww0www
                000000
                ww0www
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleBoxOnTargetLeft() {
        String map = """
                6 4
                wwwwww
                wwwwww
                +0p000
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                -0000b
                wwwwww
                """;
        String map2 = """
                6 4
                wwwwww
                wwwwww
                f000bp
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map2)));
    }
    @Test
    public void cycleBoxOnTargetRight() {
        String map = """
                6 4
                wwwwww
                wwwwww
                0000p+
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                b0000-
                wwwwww
                """;
        String map2 = """
                6 4
                wwwwww
                wwwwww
                pb000f
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(4, 2));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map2)));

    }
    @Test
    public void cycleBoxOnTargetUp() {
        String map = """
                6 4
                ww+www
                wwpwww
                000000
                ww0www
                """;
        String map1 = """
                6 4
                ww-www
                ww0www
                000000
                wwbwww
                """;
        String map2 = """
                6 4
                wwfwww
                ww0www
                00b000
                wwpwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 1));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map2)));

    }
    @Test
    public void cycleBoxOnTargetDown() {
        String map = """
                6 4
                ww0www
                ww0www
                00p000
                ww+www
                """;
        String map1 = """
                6 4
                wwbwww
                ww0www
                000000
                ww-www
                """;
        String map2 = """
                6 4
                wwpwww
                wwbwww
                000000
                wwfwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map2)));
    }
    @Test
    public void cycleTwoBoxesLeft() {
        String map = """
                6 4
                wwwwww
                wwwwww
                00p0bb
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                p000bb
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleTwoBoxesRight() {
        String map = """
                6 4
                wwwwww
                wwwwww
                bb0p00
                wwwwww
                """;
        String map1 = """
                6 4
                wwwwww
                wwwwww
                bb000p
                wwwwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(3, 2));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleTwoBoxesUp() {
        String map = """
                6 4
                ww0www
                wwpwww
                00b000
                wwbwww
                """;
        String map1 = """
                6 4
                wwpwww
                ww0www
                00b000
                wwbwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void cycleTwoBoxesDown() {
        String map = """
                6 4
                wwbwww
                wwbwww
                00p000
                ww0www
                """;
        String map1 = """
                6 4
                wwbwww
                wwbwww
                000000
                wwpwww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 1, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        field.update(new Direction(0, 1));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
    }
    @Test
    public void crossWin() {
        String map = """
                5 5
                wwfww
                wwbww
                fbpbf
                wwbww
                wwfww
                """;
        String map1 = """
                5 5
                ww+ww
                ww0ww
                +00p+
                ww0ww
                ww+ww
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 4, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, 1));

        field.update(new Direction(-1, 0));
        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        Assertions.assertTrue(field.isWinning());
    }
    @Test
    public void fakeCrossWin() {
        String map = """
                6 5
                wwfwww
                wwbwww
                fbpbf0
                wwbwww
                wwfwww
                """;
        String map1 = """
                6 5
                ww+www
                ww0www
                +p00fb
                ww0www
                ww+www
                """;
        Parser parser = new Parser();
        Field field = new Field(parser.parse(map), 4, 0, new Position(2, 2));
        field.update(new Direction(0, 1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, -1));
        field.update(new Direction(0, 1));

        field.update(new Direction(1, 0));
        field.update(new Direction(1, 0));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        field.update(new Direction(-1, 0));
        Assertions.assertTrue(Comparator.compare(field.getMap(), parser.parse(map1)));
        Assertions.assertFalse(field.isWinning());
    }
}

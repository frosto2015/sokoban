package sokoban.core;

import model.GameElement;

public class Comparator {
    public static boolean compare(GameElement[][] expect, GameElement[][] actual){
        if(expect.length != actual.length){
            return false;
        }
        for(int i = 0; i < expect.length; i++){
            if(expect[i].length != actual[i].length){
                return false;
            }
            for(int j = 0; j < expect[i].length; j++){
                if(expect[i][j] != actual[i][j]){
                    return false;
                }
            }
        }
        return true;
    }
}

package sokoban.core;

import model.GameElement;

import java.util.Arrays;

public class Parser {
    public GameElement[][] parse(String map){
        String[] stringList = map.split("\n");
        int[] size = Arrays.stream(stringList[0].split(" ")).mapToInt(Integer::parseInt).toArray();
        int width = size[0];
        int height = size[1];
        // CR(minor): префикс local в имени переменной можно и не писать :) В самом деле, в парсере же нет глобального поля.
        GameElement[][] localField = new GameElement[height][width];
        for(int i = 1; i <= height; i++) {
            for (int j = 0; j < width; j++){
                localField[i-1][j] = GameElement.fromSymbol(stringList[i].charAt(j));
            }
        }
        return localField;
    }
}
